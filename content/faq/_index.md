---
title: "Preguntas frecuentes"
date: 2023-11-20T10:00:10+06:00
description: "sadfasddfdsaffdas"
image: "/images/codex/scroll-01.jpg"
---

## ¿Por qué decidisteis crear SIAH?

Bien entrado el siglo XXI, en España no existía una iniciativa de carácter estatal que, dotada de naturaleza asociativa oficial, reuniera a las personas interesadas en aprender, compartir conocimiento y desarrollar actividades en torno al tiro con arco histórico en todas sus variantes y especialmente en el desarrollado en la península ibérica.

Al mismo tiempo, algunas personas percibíamos una creciente popularidad por ahondar en prácticas y estudios arqueros históricos. Siendo conscientes de que las instituciones actuales o bien no lo considerarán nunca como una prioridad o se centrarán primeramente en el aspecto competitivo, optamos por crear una asociación cultural que pudiera satisfacer intereses más amplios.

No podemos garantizar que esta iniciativa vaya a tener éxito más allá de unas pocas decenas de personas pero estamos determinados a luchar por conseguir que en España la arquería histórica obtenga el reconocimiento y práctica que se merece.

## ¿Por qué Ibérica y no Española?

En el mismo momento en el que otorgamos relevancia al componente histórico de la práctica del tiro con arco y sabiendo que hay registros arqueológicos datados en más de 60.000 años de antigüedad para puntas de flecha, es inevitable relativizar la importancia de un estado moderno. Sin embargo, la península ibérica constituye un entorno geográfico estable y contenido en el que igualmente nos podemos sentir identificados. Por supuesto, el archipiélago balear y el canario son absolutamente bienvenidos a SIAH y somos particularmente conscientes de las perspectivas únicas de su  naturaleza insular, mediterránea y atlántica.

Asimismo, existe una voluntad clara de sumar a nuestros hermanos portugueses a este proyecto de arquería histórica y trabajar conjuntamente en líneas de investigación y organización de tiradas en donde podamos intercambiar. SIAH es un proyecto inclusivo y tenemos al país luso en altísima estima, confiamos en poder atraerlos a este proyecto.

Finalmente, América Latina, por virtud de las lenguas que nos unen, es más que bienvenida a formar parte de SIAH, ya sea como miembros individuales atendiendo a las cuotas digitales o internacionales, o estableciendo vínculos entre diferentes asociaciones que radiquen allí. 

## ¿Cómo puedo unirme a SIAH?

El procedimiento es muy sencillo pero requiere una comunicación vía correo electrónico a secretaria@siah.ac indicando:

- Nombre y apellidos
- Correo electrónico de contacto
- DNI o documento identificativo equivalente
- Fecha de nacimiento
- Domicilio (incluye municipio)
- Teléfono de contacto
- Compañía a la que adscribirse (opcional, puede decidirse posteriormente)
- Tipo de cuota que desea satisfacer

Las cuotas que se encuentra en vigor son:

- Cuota Normal: incluye envío de ítem exclusivo anual. Suscripción en papel a nuestra revista Algarada	**30,00 €**
- Cuota Digital: no incluye envío de ítem exclusivo anual. Suscripción en pdf a nuestra revista Algarada **15,00 €**
- Cuota Internacional S/I: no incluye envío de ítem exclusivo anual. Suscripción en papel a nuestra revista Algarada	**45,00 €**
- Cuota Internacional C/I: incluye envío de ítem exclusivo anual. Suscripción en papel a nuestra revista Algarada	**65,00 €**

## ¿Puedo acceder a los estatutos de SIAH?

Naturalmente, los estatutos de una asociación cultural son un documento público en poder del Ministerio del Interior. Se pueden consultar previo pago de unas tasas. No obstante, pueden consultarse en esta misma página web a través del siguiente enlace [Estatutos SIAH](/SIAH_estatutos.pdf).

## ¿Qué es el ítem exclusivo anual?

Se trata de un objeto, de naturaleza variable pero siempre físico, que se entrega o envía cada año a los miembros de SIAH cuya cuota así lo tenga establecido. Cada año puede variar y es exclusivo de SIAH. Puede ser una prenda, un pequeño objeto decorativo o un accesorio arquero, entre otros.

## ¿En qué consiste la revista La Algarada?

La Algarada o simplemente "Algarada" es la revista oficial de SIAH en donde tanto socios de SIAH como personas ajenas a la asociación pueden enviar artículos para confeccionar cada ejemplar. En la actualidad es de periodicididad anual. Los trabajos que se aceptan deben tener relación con la arquería histórica o prehistórica y ser de interés para el resto de SIAH. Si deseas enviar un trabajo para su publicación, escribe a secretaria@siah.ac.

Algarada no es una revista que pueda encontrarse en puntos de venta y en principio es exclusiva de los socios de SIAH. No obstante, en eventos oficiales de SIAH es posible encontrar y adquirir algún ejemplar de la revista.

## ¿En qué consiste el evento anual de El Fonsado?

El Fonsado es el encuentro anual de SIAH y se compone de tres o cuatro días al año para disfrutar de charlas, talleres, cursos y, por supuesto, tiradas de todo tipo. Una fiesta para celebrar todo lo que nos emociona y une en el tiro con arco histórico. El Fonsado cuenta asimismo con una cena de gala especial denominada Farra (etimológicamente "ferra" portugués y "ferha", fiesta en árabe dialectal).

El Fonsado es un evento fundamentalmente orientado para los miembros de SIAH pero está abierto a inscripciones de gente ajena a la asociación.

El Fonsado es ante todo una reunión festiva donde nuestro día a día queda fuera, tan solo caben nuestros arcos y nuestras flechas y las ganas de pasar un gran rato juntos.

## ¿En qué consiste el Podcast "Arco con H (de histórico)"?

Se trata de un podcast grabado en formato vídeo en varios participantes se turnan para presentar temas relacionados con la arquería histórica que previamente se han preparado. La duración normal está prevista en torno a la media hora, incluyendo breves debates entre tema y tema. El primer episodio fue un especial para presentar la propia SIAH y tiene una duración mayor. Podéis disfrutar el podcast en el [canal oficial de SIAH en Youtube](https://www.youtube.com/channel/UCIlZtPcAXxE1pT2pLiVBOTQ).

## Pertenezco a un club de tiro con arco ¿podríamos formar parte de SIAH como club?

La Ley Orgánica 1/2002 que regula las asociaciones establece que una asociación cultural, como es SIAH, solo puede permitir personas físicas como socias. Por otro lado, es importante recalcar que SIAH no es una asociación deportiva, sino cultural, y el concepto de club no ha lugar. Sin embargo, SIAH sí contempla en sus estatutos el concepto de Compañía. Una Compañía es una delegación territorial de SIAH formada por al menos 5 miembros que goza de relativa autonomía. Un club deportivo podría, a través de sus miembros dados de alta en SIAH, formar una Compañía inspirada en él e integrar a otras personas en el mismo territorio, pero siempre serían entidades independientes.

## ¿Imparte SIAH cursos de tiro con arco tradicional e histórico?

SIAH como tal no imparte cursos pero de vez en cuando algunos miembros de SIAH anuncian cursos concretos sobre técnicas y materiales de tiro con arco histórico o prehistórico. Algunos ejemplos pueden ser desde talla lítica para puntas de flecha o cursos para aprender la técnica de agarre de pulgar.

## ¿Emite SIAH una licencia o tarjeta deportiva para la tenencia y uso de arcos?

SIAH es una asociación cultural y no expide documento alguno de ese tipo. Es importante que la persona que se acerque a SIAH comprenda que no somos ni pretendemos ser un sustituto de federaciones o asociaciones deportivas.

## ¿Quiere SIAH convertirse en la referencia sobre arquería histórica en la península ibérica?

Éste puede ser un tema delicado y controvertido que merece mucho la pena aclarar. SIAH sí pretende ser *una* referencia a la que acudir para tratar aspectos académicos o de práctica sobre el tiro con arco histórico. Sin embargo, no aspiramos a ser *la* referencia, como si solo en SIAH se pudieran dar las respuestas correctas a todos los temas planteados. Ni siquiera aspiramos a ser prescriptores más allá de nuestros muros asociativos aunque sí queremos ofrecer unas garantías de criterio y rigor en aquello que difundamos y defendamos como asociación. Ya sean las actividades de corte más erudito como tiradas absolutamente lúdicas rendirán cuentas exclusivamente a los miembros de SIAH, que serán quienes las promuevan dentro de una sana pluralidad y apertura de mente. 

Consideramos que igual que debe resultar compatible cuidar y proteger un trabajo académico y de investigación con un disfrute sin prejuicios de la práctica arquera histórica, también podemos aspirar a que la actividad desarrollada dentro de SIAH pueda aportar a mucha otra gente al margen de nuestra asociación. 

## ¿De dónde proceden muchas de las ilustraciones de la web de SIAH?

Son obra de Juan de la Cruz @elhombretecla y del proyecto de ilustraciones libres [The Lost Abbey Archive](https://lostabbeyarchive.com). Juan de la Cruz es arquero y simpatizante de SIAH, además de amigo de varios de sus integrantes. Agradecemos a Juan su apoyo a SIAH en forma del grandísimo trabajo artístico que nos ha proporcionado.


## ¿Cómo puedo contactar con SIAH?

El correo electrónico más obvio es secretaria@siah.ac. Aparte, en Instagram estamos en https://www.instagram.com/siah.ac/ y en Facebook en https://www.facebook.com/SIAH.iberica.

