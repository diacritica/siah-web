---
title: "El Fonsado 2022"
date: 2032-03-21T08:00:00+02:00
description: "Evento anual de SIAH"
type: "post"
image: "images/logofonsado22.jpg"
categories: 
  - "Evento"
tags: null
published: false
---

Tras dos años de vida, SIAH está de enhorabuena al poder celebrar un encuentro anual largamente esperado. La Compañía del Dragón organizará El Fonsado el fin de semana del 6 al 8 de mayo en donde esperamos dar la bienvenida a las compañías del Torb, del Falcó y la Errante así como simpatizantes de SIAH y la arquería histórica y prehistórica.

Este evento, celebrado en el municipio de la Sierra de Guadarrama, Los Molinos, contará con una docena de pruebas arqueras, conferencias, talleres y hasta una cena de gala el sábado noche. 

**ACTUALIZACIÓN 10 mayo 2022:** El Fonsado tuvo lugar y fue una experiencia increíble, puedes leer [una de las crónicas con fotos y vídeo aquí](https://blog.aljaba.net/cronica-de-el-fonsado-de-siah-2022/).



