---
title: "Podcast 'Arco con H'"
date: 2030-10-01T14:46:10+06:00
description: "NOSEUSA"
type: "post"
image: "images/codex/mirror-03.jpg"
categories: 
  - "Podcast"
tags: null
---

¡Tenemos primer episodio de nuestro podcast "Arco con H *de histórico*"! Es el [podcast oficial de SIAH](https://www.youtube.com/channel/UCIlZtPcAXxE1pT2pLiVBOTQ) y en este primer encuentro cubrimos sobre todo aspectos de la propia asociación cultural.

Fer, Angela, Haritz, Pablo e Ismael hablarán sobre arquería histórica, el *estilo* de SIAH como asociación cultural, la inspiración de la que bebemos y, naturalmente, cuestiones del funcionamiento interno de SIAH y recursos disponibles.

Se trata de un primer episodio de duración especial, más largo que el formato que se ha decidido, en donde en lugar de disponer de 5-6 minutos por tema, tenemos cerca de 10. Esperamos que paséis un rato entretenido y [os suscribáis](https://www.youtube.com/channel/UCIlZtPcAXxE1pT2pLiVBOTQ) para estar al tanto de los nuevos episodios que iremos publicando.

{{< youtube -ZFNb_hpLNw >}}



