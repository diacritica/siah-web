---
title: "El Fonsado 2023"
date: 2033-02-01T08:00:00+02:00
description: "Evento anual de SIAH"
type: "post"
image: "images/fonsado23/logofalco.png"
categories: 
  - "Evento"
tags: null
---

¡El Fonsado vuelve! Tras el extraordinario éxito del año pasado, este año es la Companyia del Falcó quienes nos harán disfrutar de un largo fin de semana de pruebas de arquería inspiradas en las diferentes culturas arqueras del mundo. Muy importante también, el ambiente que se respira entre la gente que asiste es sencillamente lo mejor de este encuentro de arquería histórica y prehistórica.

Este evento, celebrado en el municipio de Pontós en Gerona y contará con una decena de pruebas arqueras, conferencias, talleres y hasta una cena de gala el sábado noche. 

**ACTUALIZACIÓN 4 mayo 2023:** Apenas quedan ya plazas. Asegúrate de conseguir la tuya antes del domingo 21 de mayo consultando la información [aquí](/fonsado).



