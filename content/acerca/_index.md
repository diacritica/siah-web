---
title: "Acerca de SIAH"
date: 2023-11-20T10:00:10+06:00
description: "sadfasddfdsaffdas"
image: "images/brand/banner.jpg"
---

> “El tiro con arco es un arte, disparar un arco es una fiesta y acertar es una alegría.”
> **Ibn Hudayl**

El tiro con arco ha sido una actividad estrechamente relacionada con la humanidad desde tiempo inmemorial: como herramienta fundamental para la supervivencia, como arma decisiva en innumerables momentos históricos, como actividad recreativa e incluso como deporte. Esta versatilidad ha permitido que nacieran y florecieran numerosas tradiciones alrededor de este arte en todo el mundo. Cómo no, nuestro país no ha sido ajeno a esta tendencia y el tiro con arco ha demostrado su importancia en numerosas ocasiones.

Pese a ello, en la actualidad la arquería histórica es una práctica casi olvidada. Incluso cuando se ha pretendido resucitar esta actividad, la mayoría de las veces se ha hecho de forma que ha pesado más el carácter estético o lúdico que el rigor por la metodología, usos y costumbres de la época.

![Asistentes al Fonsado 2022, evento anual de SIAH](/images/fonsado.jpg)


## Objetivo

Denominamos arco clásico, histórico o primitivo, a aquel arco cuyo diseño y uso aceptado sea anterior al año 1900. Además del diseño, los materiales del arco o material relacionado, deberán estar en consonancia con el periodo histórico que representen (salvo que, por motivos de seguridad, se permita o recomiende el uso de materiales modernos o anacrónicos).

El objetivo de la Sociedad Ibérica de Arquería Histórica (en adelante SIAH) es el estudio y difusión en la península ibérica de estos arcos, sus características y las técnicas adecuadas para su correcta utilización; así como de la época y el trasfondo asociado a cada uno de ellos. SIAH no está vinculada a ninguna organización relacionada con el tiro con arco. 

## Visión

En la península ibérica el panorama del tiro con arco actual está centrado en su aspecto más competitivo. Esto, junto a llegada de las nuevas tecnologías aplicadas a esta actividad, son el modelo predominante y relegan cualquier otra opción a un lugar secundario, en el mejor de los casos. Por eso, creemos que es necesaria la existencia de una entidad que defienda y de visibilidad al tiro con arco histórico y prehistórico dentro del modelo actual. Una organización donde poder estudiar y practicar con rigor la arquería histórica; un lugar que reúna todo ese conocimiento para todo aquel que quiera iniciarse o profundizar en la arquería histórica. Una plataforma que nos permita, no solo establecer una red de intercambio de estos conocimientos con organizaciones afines, ya sea a nivel nacional o internacional, sino dar una especial visibilidad a la tradición arquera en nuestra península.

## Misión

Para alcanzar los objetivos que SIAH ha fijado, consideramos que es fundamental:

- el estudio de fuentes primarias, es decir, conocer y mostrar los ejercicios y técnicas de tiro utilizadas por nuestros antepasados, así como un acercamiento al contexto histórico en cuestión.
- la creación de un archivo veraz y riguroso, que nos ayude a difundir toda esta valiosa información.
- la puesta en práctica de dichas enseñanzas, en un ambiente distendido que no busque tanto la competición deportiva sino la reivindicación de una actividad histórica, a través de pruebas representativas de la época y del estilo de arco en cuestión.
- fomentar las relaciones con otros grupos afines a nuestro ideario, ya sean organizaciones que provengan del tiro con arco, de estudios históricos, de otras disciplinas de corte histórico o recreacionista, tanto nacionales como extranjeras, con el propósito de establecer vínculos que nos permitan crecer y avanzar en nuestros propósitos.

## Valores

Los valores que definen a SIAH, son:

- Dignidad: reivindicamos el tiro con arco histórico en todas y cada una de sus variantes, dentro del marco actual.
- Transparencia: honradez y claridad en todas las gestiones y comportamientos de SIAH.
- Igualdad y universalidad: actuamos en común para aportar conocimiento y responder a las necesidades colectivas. Nos interesa trabajar en todo lo que sea de interés de los integrantes de nuestro colectivo.
- Inclusión y cooperación: nuestros esfuerzos van dirigidos a todo aquel que esté interesado en formar parte de SIAH o colaborar con ella, sin importar sus conocimientos sobre historia o tiro con arco. Buscamos el compromiso y colaboración pública. Nos interesa lo que ocurre en otras asociaciones afines y queremos ser un activo en la consecución de fines conjuntos.
- Liderazgo compartido y participación: todos tenemos algo que aportar, todos somos participes de nuestro proyecto común.
- Eficiencia, calidad y mejora continua: alcanzamos objetivos y metas programadas mediante la óptima utilización de los recursos y fuentes disponibles. Es nuestra
responsabilidad para satisfacer las necesidades los miembros de SIAH, buscar permanentemente información veraz, rigurosa y de calidad.
- Independencia: somos independientes respecto a federaciones y otras entidades rectoras del tiro con arco en nuestro país, lo que nos permite trabajar en la consecución de nuestros objetivos, evitando cualquier otro tipo de interés que no sea el de la propia SIAH.
- Innovación y Creatividad: necesitamos asumir los cambios y promoverlos, estimular nuevos comportamientos, atendiendo las nuevas necesidades generadas y ofertando soluciones de futuros.
- Interculturalidad: compartimos, intercambiamos y aprendemos de y con otras organizaciones, tanto nacionales como extranjeras, para el beneficio y enriquecimiento común.
- Sentido de pertenencia: es de gran valor que cada una de las personas integrantes de SIAH se sientan miembros y parte de un "nosotros".

-----------------------------------

Si deseas formar parte de SIAH, envía un correo a secretaria@siah.ac con la siguiente información:

- Nombre y apellidos
- Correo electrónico de contacto
- DNI o documento identificativo equivalente
- Fecha de nacimiento
- Domicilio (incluye municipio)
- Teléfono de contacto
- Compañía a la que adscribirse (opcional, puede decidirse posteriormente)
- Tipo de cuota que desea satisfacer

Las cuotas que se encuentra en vigor son:

- Cuota Normal: incluye envío de ítem exclusivo anual. Suscripción en papel a nuestra revista Algarada	**30,00 €**
- Cuota Digital: no incluye envío de ítem exclusivo anual. Suscripción en pdf a nuestra revista Algarada **15,00 €**
- Cuota Internacional S/I: no incluye envío de ítem exclusivo anual. Suscripción en papel a nuestra revista Algarada	**45,00 €**
- Cuota Internacional C/I: incluye envío de ítem exclusivo anual. Suscripción en papel a nuestra revista Algarada	**65,00 €**

La cuota normal aplica a Portugal, Andorra y a España.
