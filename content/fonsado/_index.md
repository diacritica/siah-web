---
title: "Evento El Fonsado 30 de mayo a 1 de junio 2025"
date: 2025-02-15T10:00:10+06:00
description: "Información sobre El Fonsado de SIAH 2025"
image: "/images/fonsado25/foto.jpg"
---


El Fonsado es el **encuentro anual de SIAH** en donde toda la gente asistente disfruta de un fin de semana largo de actividades arqueras, talleres, conferencias y fraternidad arquera. 

[Formulario de inscripción abierto hasta el 15 de abril](https://docs.google.com/forms/d/e/1FAIpQLSco2qg1ybOul9s3zslVGytWku3dO6CQAH-Pqn-Tx0RiaurrSQ/viewform?usp=preview). ¡Plazas muy limitadas!

El leitmotiv del Fonsado 2025 es: *Prehistoria e Historia arqueras en la Península Ibérica; 4 momentos*.

![](/images/fonsado/banner3fotos.jpg)
*Diferentes épocas, diferentes gentes*


Efectivamente, celebraremos el neolítico, el Imperio Romano, el califato de Córdoba en Al-Andalus y la batalla de Aljubarrota en el 1385.

Lugar: instalaciones del [Albergue San Agustín](https://www.alojamientoruralsanagustin.com/) en Fuentelencina, Guadalajara, España.

[Enlace a Google Maps](https://maps.app.goo.gl/YSohHt3ZGEiXmBCn6).

![](/images/fonsado.jpg)
*Salva inaugural en el Fonsado*


## Opciones de alojamiento

- De viernes a domingo (miembros SIAH): pensión completa y actividades - 155€
- De viernes a domingo (si no eres miembro de SIAH): pensión completa y actividades - 170€

El alojamiento es en cabañas y habitaciones a elegir entre 2 a 6 personas con baño en-suite y todas las comodidades de un albergue rural moderno. El comedor tiene una estupenda cocina propia, salas de conferencias (y de fiesta), lugares de esparcimiento en el exterior y a resguardo y zonas habilitadas para la práctica del tiro con arco, talleres al aire libre, etc.

Si te llama la atención la arquería histórica o prehistórica y le das importancia a un ambiente distendido, variado y llenos de camaradería, ¡no lo dudes! Apúntate al Fonsado aunque no seas de SIAH. Si quieres hacerte una idea de qué es un Fonsado, [puedes leer una crónica del Fonsado 2022 aquí](https://blog.aljaba.net/cronica-de-el-fonsado-de-siah-2022/).

[Formulario de inscripción abierto hasta el 15 de abril](https://docs.google.com/forms/d/e/1FAIpQLSco2qg1ybOul9s3zslVGytWku3dO6CQAH-Pqn-Tx0RiaurrSQ/viewform?usp=preview). ¡Plazas muy limitadas!

## Fotografías de anteriores fonsados

![](/images/fonsado25/talleres.jpg)
*Un taller en el Fonsado 2024, organizado por la Compañía Edetanos*

![](/images/fonsado25/farra.jpg)
*La Compañía dl Dragón en un momento previo a la Farra, la cena de gala del Fonsado*


## Fotografías del albergue y alrededores

![](/images/fonsado25/albergue01.jpg)
*Vista del pueblo desde el albergue*

![](/images/fonsado25/albergue02.jpg)
*Interior del recinto, vista de las cabañas independientes*

![](/images/fonsado25/albergue03.jpg)
*Estupendo comedor*

![](/images/fonsado25/albergue04.jpg)
*Una de las múltiples habitaciones con opciones para 2, 3, 4, 5 y 6 personas con baño(s) incluido(s)*

![](/images/fonsado25/albergue05.jpg)
*Pradera sur*

![](/images/fonsado25/albergue06.jpg)
*Camino de recepción*