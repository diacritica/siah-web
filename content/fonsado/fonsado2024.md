---
title: "Evento El Fonsado 10-12 mayo 2024"
date: 2024-01-28T10:00:10+06:00
description: "Información sobre El Fomsado de la Compañía Edetanos"
image: "/images/fonsado24/foto.jpg"
---

**Actualización: ¡Inscripciones cerradas! ¡Nos vemos en El Fonsado!**


El Fonsado es el **encuentro anual de SIAH** en donde toda la gente asistente disfruta de un fin de semana largo de actividades arqueras, talleres, conferencias y fraternidad arquera. 

El leitmotiv del Fonsado 2024 organizado por la Compañía Edetanos (Comunidad Valenciana) es **"Sagitta Vitae, flechas para la vida"**.

Lugar: instalaciones del complejo ACTIO en [Alborache](https://maps.app.goo.gl/snB3DwQmmRWfTfdF6).


**Opciones de alojamiento**

- De viernes a domingo (miembros SIAH): pensión completa y actividades - 155€
- De viernes a domingo (si no eres miembro de SIAH): pensión completa y actividades - 175€
- Solo sábado: pensión completa, alojamiento y actividades - 110€
- Solo sábado: pensión completa y actividades - 90€


Si te llama la atención la arquería histórica o prehistórica y le das importancia a un ambiente distendido, variado y llenos de camaradería, ¡no lo dudes! Apúntate al Fonsado aunque no seas de SIAH. Si quieres hacerte una idea de qué es un Fonsado, [puedes leer una crónica del Fonsado 2022 aquí](https://blog.aljaba.net/cronica-de-el-fonsado-de-siah-2022/).

![Una foto del fantástico equipo organizador](/images/fonsado24/edetanos_organizador.jpg)
*Una foto del equipo organizador en una visita reciente a las instalaciones*