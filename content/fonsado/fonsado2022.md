---
title: "Evento El Fonsado 6-8 mayo 2022"
date: 2022-02-18T10:00:10+06:00
description: "sadfasddfdsaffdas"
image: "/images/fonsado/embudo_con_logo.jpg"
---
El Fonsado es el encuentro anual de SIAH en donde toda la gente asistente disfruta de un fin de semana largo de actividades arqueras, talleres, conferencias y fraternidad arquera. El correo de contacto es fonsado@siah.ac. El Fonsado está abierto a todo el mundo, sea miembro de SIAH o no.

##### [ACTUALIZACIÓN 10 mayo 2022] El Fonsado tuvo lugar y fue una experiencia increíble, puedes leer [una de las crónicas con fotos y vídeo aquí](https://blog.aljaba.net/cronica-de-el-fonsado-de-siah-2022/).


###### [Actualización 5 de abril de 2022] Incorporamos la Guía Rápida de Preparación para El Fonsado [para su consulta](/images/fonsado/guia_rapida_preparacion_fonsado.pdf). La versión definitiva se hará llegar una semana antes del evento.

#### Programa preliminar de actividades

A falta de una agenda detallada, están confirmadas las siguientes actividades:

![s](/images/fonsado/agenda.jpg)

##### Sobre la cena de gala

Se trata de una cena en la que los comensales podrán lucir sus mejores galas a juego con sus arcos. Si tienes un fondo de armario recreacionista o te has hecho con un atuendo que podría pasar por el de un arquero de una época determinada, estaremos encantados de verte lucir tu modelito, Pero por favor, ten en cuenta que las prendas de poliéster y el “brilli-brilli” en general no se
consideran ropa histórica (ni a veces “ropa” a secas). No se trata de disfrazarse, sino de vestirse según una época determinada.

Obviamente no es algo obligatorio: si no tienes tiempo, o simplemente no te apetece, porque lo de recrear no es lo tuyo, no pasa nada, puedes venir de arquero del siglo XXI, lo pasarás igual de bien.


![s](/images/fonsado/banner3fotos.jpg)

*En SIAH damos la bienvenida a toda práctica arquera histórica y prehistórica. En la foto, Fer, Haritz y Angela con tres arcos diferentes*

#### Fechas de El Fonsado

El Fonsado dará comienzo a las **17h del viernes 6** y concluirá a las **14h del domingo 8 de mayo**.

![s](/images/fonsado/presidenta.jpg)

*María es la Presidenta de la Compañía del Dragón. Ésta es la Compañía encargada de la organización de El Fonsado este año*

#### Lugar de celebración

Para El Fonsado de 2022 contaremos con las excelentes instalaciones del albergue "El Espacio de los Molinos" del Grupo Colladito, en el municipio de Los Molinos en la sierra de Guadarrama en Madrid.

La dirección exacta es: Calle Ave María 3, 28460 Los Molinos, Madrid. 

Podéis consultarlo en [Google Maps](https://goo.gl/maps/a35jcUBjFw9jQ8WT9)

![s](/images/fonsado/exteriores.jpg)

*Parte de la finca rústica en donde disfrutaremos de las actividades arqueras que requieran más espacio*

![s](/images/fonsado/tirolina.jpg)

*El espacio cuenta con muchas oportunidades para retos de puntería nada habituales*


![s](/images/fonsado/conferencia.jpg)

*Contamos con una sala de conferencia cómodo y completamente equipada*

![s](/images/fonsado/esparcimiento.jpg)

*Tanto en los descansos como para quienes quieran alargar la jornada por la noche, contamos con todo lo necesario para relajarnos*

![s](/images/fonsado/campos.jpg)

*El terreno dentro del albergue se presta a paseos y talleres prácticos al aire libre*


#### Precios para El Fonsado

![s](/images/fonsado/precios.png)

¿Qué incluye cada forma de asistencia?

- El alojamiento lo incluye todo: alojamiento, acceso a las actividades, talleres y charlas, manutención (desayunos, comidas y cenas). Todas las habitaciones tienen baño incluido y pueden configurarse para uso individual o doble.
- La asistencia no lo incluye todo: ni alojameinto ni desayunos, pero sí acceso a las actividades, talleres y charlas, así como las comidas y cenas.

El resto, depende de si se asiste desde el viernes (muchas cosas arqueriles que haremos el viernes por la noche, momentos inolvidables) o desde el sábado.

Menores de 6 años, pagan la mitad y menores de 3 años asisten de forma gratuita. ¡El Fonsado puede ser un gran plan para toda la familia!

![s](/images/fonsado/prehistoria.jpg)

*¡Qué bonitas son las flechas!*

#### Proceso de inscripción

Para poder asistir a El Fonsado de SIAH es necesario pre-inscribirse en el siguiente [formulario](https://docs.google.com/forms/d/e/1FAIpQLScaNo7YX9MTXE46JOeltwxvTyD9EGrs6N3Dfn6yI0fLnhJhHQ/viewform?usp=sf_link). Posteriormente se recibirá el importe total a ingresar para confirmar la pre-inscripción. Tenemos plazas limitadas ¡no lo dejes para el último momento!

En el formulario de pre-inscripción se hace mención a la posibilidad de que acudan acompañantes que no participen de las actividades de El Fonsado.

El Fonsado está planteado como un evento de encuentro para los miembros de la asociación. Hacerse de SIAH es muy fácil y rápido y tan solo cuesta 15€ al año en su cuota más económica. Si estás interesado en formar parte de SIAH a tiempo de El Fonsado, simplemente rellena el formulario de inscripción y deja un comentario relativo a tu deseo de hacerte socio de SIAH.


#### ¿Tienes dudas sobre si deberías asistir a El Fonsado?

¿Te gusta la historia, la prehistoria y el tiro con arco? ¿Te gustan especialmente cuando compartes estas pasiones con gente como tú? ¿Cuentas con un arco de inspiración histórica o prehistórica o planeas hacerte con uno en breve? ¡Pues vente a El Fonsado! No hay otro evento igual en la península ibérica. Tendrás todas las oportunidades para tirar con arco en modalidades nada habituales, aprender con charlas y talleres, disfrutar del equilibrio entre lo erudito y lo lúdico y desconectar 3 días en plena naturaleza en el mes de mayo sin más horario que lo que te pida el cuerpo.

Escríbemos a fonsado@siah.ac si tienes alguna duda sobre tu posible asistencia, prometemos responder rápidamente. 

El Fonsado 2022 de SIAH está organizado por la Compañía del Dragón y su lema es ¡Siempre una flecha más!

![s](/images/fonsado/escudo_dragon_oficial.png)

##### Entidades colaboradoras

Queremos agradecer al club [Arqueros de Madrid](https://arquerosdemadrid.net/) su colaboración desinteresada al proporcionarnos las dianas 3D que se emplearán durante El Fonsado.
