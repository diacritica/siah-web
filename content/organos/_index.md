---
title: "Órganos de SIAH"
date: 2020-10-22T10:00:10+06:00
description: "sadfasddfdsaffdas"
image: "/images/codex/inn-01.jpg"
---
SIAH tiene un número muy concreto de estamentos de representación y grupos de trabajo. A continuación se listan todos los que existen en la actualidad.

#### Órganos permanentes de SIAH

##### Asamblea General

Se celebra al menos una vez al año de forma ordinaria y pueden acudir todas las personas socias de pleno derecho de SIAH. Se votan aspectos importantes del funcionamiento de SIAH y es el órgano de mayor legitimidad de la asociación. En la Asamblea General se votan presupuestos anuales, elección de equipos de Algarada o El Fonsado, cambios en los estatutos o reglamentos internos y se debate la situación actual de SIAH, entre otros.

##### Comisión Permanente

El órgano de gobierno elegido por la Asamblea General que ostenta los cargos de representación y gestión de SIAH. Se conforma al menos por Presidencia, Vicepresidencia, Secretaría, Tesorería y un número de vocalías. En la actualidad está compuesta de la siguiente forma:

- Presidencia: María Martínez Cacicedo
- Vicepresidencia: Haritz Solana Galán
- Secretaría: Pablo Ruiz Múzquiz
- Tesorería: Angela Rivera Campos
- Vocalía de relaciones internacionales: Dakian Vasile Muntean
- Vocalía de relaciones con asociaciones en la península ibérica: Sergi Pastor Bravo

##### Junta Directiva

Es un órgano de representación de inspiración territorial y sectorial. Se conforma por la Comisión Permanente, presidencias de Compañías, presidencias de Gremios y Bibliotecario. Resuelve numerosos puntos operativos dentro de SIAH aunque algunas de sus decisiones pueden necesitar refrendo en futuras Asambleas Generales. Una persona miembro de la Junta Directiva puede asistir en calidad de oyente a las reuniones de la Comisión Permanente.

##### Compañía Errante

Se trata de una Compañía de personas socias que acoge a todo aquel que no se decanta por ninguna de las Compañías que operan en su región geográfica.

#### Órganos no permanentes de SIAH

##### Compañías

Son agrupaciones de socios de SIAH que tienen una delimitación geográfica apropiada para sus actividades, como puede ser una municipio, una provincia o una comarca. Disfrutan de entidad propia, representación en la Junta Directiva y autonomía para organizar sus propias actividades, sean presenciales o no. El mínimo número para poder constituir o mantener una Compañía es 5 personas.

En la actualidad existen las siguientes Compañías.

- Compañía Errante: Presidente Haritz Solana Galán errante@siah.ac
- Compañía del Dragón: Presidenta María Martínez Cacicedo dragon@siah.ac. Abarca Comunidad de Madrid y territorios adyacentes.
- Companyia del Falcó: Presidente Sergi Pastor Bravo falco@siah.ac. Abarca Catalunya.
- Compañía Nazarí: Presidente Daniel Gamez de la Hoz nazari@siah.ac. Abarca Andalucía.
- Companyia del Torb: Presidenta Sara Ubach Balagué torb@siah.ac. Abarca Andorra.
- Compañía de la Montaña: Presidente Francisco Hernandez Gonzalez montana@siah.ac. Abarca Cantabria.

##### Gremios

Son agrupaciones de socios de SIAH que comparten intereses comunes en torno a una disciplina concreta, sin importar su ubicación geográfica o pertenencia a una compañía y otros gremios. Disfrutan de entidad propia, representación en la Junta Directiva y autonomía para organizar sus propias actividades, sean presenciales o no. El mínimo número para poder constituir o mantener un Gremio es 5 personas.

- CÓDICE. Gremio de Coordinación Académica: Maestra Ester Torredelforth
- Heraldos de SIAH. Gremio de Comunicación: Maestro Ismael Hernández Rueda
- La Cierva Roja. Gremio de Prehistoria. Maestra Pilar Machín Aguilera
- Lino y Lana. Gremio de Recreación. Maestro Félix Gallego Guerrero 


##### Grupo de trabajo de la revista Algarada

Este grupo de trabajo acomete el proyecto vinculado a la candidatura que presentó ante la Asamblea General y que resuelve la edición y distribución de la revista propia de SIAH, Algarada.

##### Grupo de trabajo de El Fonsado

Este grupo de trabajo acomete el proyecto vinculado a la candidatura que presentó ante la Asamblea General y que resuelve la preparación y celebración del encuentro anual de SIAH, El Fonsado.


