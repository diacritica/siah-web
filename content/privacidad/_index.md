---
title: "Política de Privacidad"
date: 2020-10-22T10:00:10+06:00
description: "sadfasddfdsaffdas"
image: ""
---

## Información Protección de Datos – Política de Privacidad

#### ¿Quién es el Responsable del tratamiento de sus datos?

Razón social: SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA)   
Número de inscripción estatal: 620.636  
CIF: G0276867  
Dirección (notificaciones): Calle Dieg de León 16, Piso 2 Puerta 7, 28006 Madrid, España  
Email: secretaria@siah.ac  

#### ¿Con qué finalidad tratamos sus datos personales?

En SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) tratamos la siguiente información y con las siguientes finalidades:

##### SER MIEMBRO

Para la gestión de tu membresía a nuestra asociación, comunicaciones oficiales relativas a la pertenencia a la asociación así como relativas al funcionamiento de ésta.

##### CONTACTO

Gestionar los aspectos relativos a consultas planteadas, y envíos sobre la asociación.

#### ¿Por cuánto tiempo conservaremos sus datos?

##### SER MIEMBRO

Todo el tiempo que seas socio, y las necesidades legales que pudieran surgir de dicha relación.


#### ¿Cuál es la legitimación para el tratamiento de sus datos?

##### SER MIEMBRO

Consentimiento del interesado al realizar la inscripción.


#### ¿A qué destinatarios se comunicarán sus datos?

No se comunicarán datos fuera de los miembros de SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA), salvo obligación legal.

#### ¿Cuáles son sus derechos cuando nos facilita sus datos?

Cualquier persona tiene derecho a obtener confirmación sobre si en SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) estamos tratando datos personales que les conciernan o no.

Las personas interesadas tienen derecho a si acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.

En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones.

En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos.

SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) dejará de tratar los datos, salvo por motivos legítimos imperiosos o el ejercicio o la defensa de posibles reclamaciones.

El interesado tiene derecho a presentar una reclamación ante la Autoridad de control (agpd.es) si considera que el tratamiento no se ajusta a la normativa vigente.

###### Datos de contacto para ejercer sus derechos

SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA)  
Pablo Ruiz Múzquiz  
c/ Ayala 156 BAJO IZQ B  
28009 Madrid

O a través de correo electrónico a presidencia@siah.ac, junto con prueba válida en derecho, como fotocopia del DNI e indicando en el asunto «PROTECCIÓN DE DATOS».

#### ¿Cómo hemos obtenido sus datos?

Los datos personales que tratamos en SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) han sido facilitados por el propio interesado.

El interesado que envía la información a SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) garantiza y responde, en cualquier caso, de la exactitud, vigencia y autenticidad de los datos personales facilitados y se comprometen a mantenerlos debidamente actualizados, exonerándose la SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) de cualquier responsabilidad al respecto. El usuario acepta proporcionar información completa y correcta en los formularios de registro.

Asimismo, SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) no responde de la veracidad de las informaciones que no sean de elaboración propia y de las que se indique otra fuente, por lo que tampoco asume responsabilidad alguna en cuanto a hipotéticos perjuicios que pudieran originarse por el uso de dicha información.

#### Uso común con redes sociales

Si el afectado opta por acceder a los Servicios a través de su cuenta en las redes sociales (es decir, Facebook, Twitter o Instagram) o hace clic en uno de los botones de conexión o vínculos de las redes sociales (por ejemplo, el botón «Me gusta» de Facebook) que están disponibles en los Servicios, su contenido y su información personal serán compartidos con las correspondientes redes sociales.

El afectado es consciente de, y acepta, que el uso de su información personal, incluida la información que comparta con las redes sociales a través de los Servicios, por parte de las redes sociales, se rige por sus respectivas políticas de privacidad. Si no desea que las redes sociales recopilen su información, examine la política de privacidad de la correspondiente red social y/o desconéctese de la misma antes de utilizar nuestros Servicios.

#### Menores

Los menores de entre 14 y 16 años no podrán registrarse salvo que cuenten con la autorización de sus padres y/o tutores legales.

#### Cambios en la Política de Privacidad

SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) se reserva el derecho a modificar la presente política para adaptarla a novedades legislativas o jurisprudenciales. En dichos supuestos SIAH (SOCIEDAD IBÉRICA DE ARQUERÍA HISTÓRICA) anunciará en esta página los cambios introducidos con razonable antelación a su puesta en práctica.
