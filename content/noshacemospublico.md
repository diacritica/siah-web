---
title: "SIAH se hace público"
date: 2030-10-23T08:00:00+02:00
description: "Medieval Bows and arrows"
type: "post"
image: "images/logoSIAHvert.jpg"
categories: 
  - "Anuncio"
tags: null
---

¡Hacemos pública la existencia de SIAH! Tras una larga espera para asegurarnos de que podíamos anunciarlo correctamente, midiendo nuestras fuerzas, y con todos los parabienes del Ministerio del Interior de España, hoy 24 de octubre nos "activamos" de cara al público en general. Como asociación cultural de ámbito estatal inscrita con el número 620636, podemos operar ya con total normalidad y celebrar las primeras reuniones para tramitar aspectos meramente burocráticos (pero relevantes) y poder recibir solicitudes de adhesión a la asociación.

Para conocer un poco más a SIAH, visita la sección [Acerca de SIAH](/acerca/) en nuestra web y también accede al [Códex](https://codex.siah.ac).

> Si deseas formar parte de SIAH, escríbemos para informarte a info@siah.ac ¡Te esperamos!



